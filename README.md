# README #
Elecciones

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
NGINX Server 

* Configuration Server

server {
    listen       80;
    root       /var/www/telemetrio;

    index index.php index.html index.htm;

    access_log  /usr/local/etc/nginx/logs/default.access.log  main;

    error_page  404     /404.html;
    error_page  403     /403.html;

        location / {
                if ($request_uri !~ (assets|jpe?g|swf|gif|png|mp4|ogg|webm|js|css|bmp|pptx|images|robots\.txt|index\.php.*)){
                        rewrite ^/(.*)$ /index.php/$1 last;
                }

        error_page 405 = $uri;
        }

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        location ~ \.php$ {
                fastcgi_read_timeout 1080;
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                try_files $uri =404;
                fastcgi_pass 127.0.0.1:9000;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
        }

        location ~ /index.php {
                fastcgi_read_timeout 1080;
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                fastcgi_pass 127.0.0.1:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
                #fastcgi_param  REQUEST_URI      $request_uri;
                #fastcgi_param  QUERY_STRING     $query_string;
                #fastcgi_param  REQUEST_METHOD   $request_method;
                #fastcgi_param  CONTENT_TYPE     $content_type;
                #fastcgi_param  CONTENT_LENGTH   $content_length;
                include fastcgi_params;
        }

        # deny access to .htaccess files, if Apache's document root
        location ~ /\.ht {
                deny all;
        }
}

* Database
- MySQL 5.7.9 (Import the database "elecciones.sql")

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Test
To run the script execute http://localhost/elecciones/onpe

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact