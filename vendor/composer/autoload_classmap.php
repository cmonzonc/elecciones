<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Bootsole\\ButtonBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/componentbuilder.php',
    'Bootsole\\ButtonDropdownAddonBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/componentbuilder.php',
    'Bootsole\\ButtonGroupBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/componentbuilder.php',
    'Bootsole\\DropdownBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/componentbuilder.php',
    'Bootsole\\DropdownButtonBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/componentbuilder.php',
    'Bootsole\\FormBootstrapRadioBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormButtonBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormCheckboxFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormColorFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormComponentBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormComponentCollectionBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormEmailFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormFieldAddonable' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormFieldOptionBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormFieldSelectable' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormFieldSelectableItem' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormGroupBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormHiddenFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormNumberFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormPasswordFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormRadioFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormSearchFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormSelect2FieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormSelectFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormSelectTimeFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormSwitchFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormTextAreaFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormTextFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormToggleFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\FormUrlFieldBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/formbuilder.php',
    'Bootsole\\HtmlAttributesBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/htmlbuilder.php',
    'Bootsole\\HtmlBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/htmlbuilder.php',
    'Bootsole\\ItemCollection' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/componentbuilder.php',
    'Bootsole\\MenuItemBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/componentbuilder.php',
    'Bootsole\\NavBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/navbuilder.php',
    'Bootsole\\NavButtonBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/navbuilder.php',
    'Bootsole\\NavComponentBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/navbuilder.php',
    'Bootsole\\NavDropdownBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/navbuilder.php',
    'Bootsole\\NavFormBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/navbuilder.php',
    'Bootsole\\NavLinkBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/navbuilder.php',
    'Bootsole\\NavTextBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/navbuilder.php',
    'Bootsole\\NavbarBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/navbuilder.php',
    'Bootsole\\PageBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/pagebuilder.php',
    'Bootsole\\PageFooterBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/pagebuilder.php',
    'Bootsole\\PageHeaderBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/pagebuilder.php',
    'Bootsole\\PageSchema' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/pagebuilder.php',
    'Bootsole\\TableBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/tablebuilder.php',
    'Bootsole\\TableColumnBuilder' => $vendorDir . '/alexweissman/bootsole/bootsole/htmlbuilder/tablebuilder.php',
    'Bootsole\\ValidationSchema' => $vendorDir . '/alexweissman/bootsole/bootsole/validation/validation.php',
);
