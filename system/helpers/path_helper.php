<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Path Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/path_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('set_realpath'))
{
	/**
	 * Set Realpath
	 *
	 * @param	string
	 * @param	bool	checks to see if the path exists
	 * @return	string
	 */
	function set_realpath($path, $check_existance = FALSE)
	{
		// Security check to make sure the path is NOT a URL. No remote file inclusion!
		if (preg_match('#^(http:\/\/|https:\/\/|www\.|ftp)#i', $path) OR filter_var($path, FILTER_VALIDATE_IP) === $path )
		{
			show_error('The path you submitted must be a local server path, not a URL');
		}

		// Resolve the path
		if (realpath($path) !== FALSE)
		{
			$path = realpath($path);
		}
		elseif ($check_existance && ! is_dir($path) && ! is_file($path))
		{
			show_error('Not a valid path: '.$path);
		}

		// Add a trailing slash, if this is a directory
		return is_dir($path) ? rtrim($path, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR : $path;
	}
	
	/**
	* Path USER IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function user_api_image($value = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('user_api_image_path') . $value;
	}

	/**
	* Path TEAM IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function team_api_image($value = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('team_api_image_path') . $value;
	}
		
	/**
	* Path MULTIMEDIA IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function multimedia_api($value = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('multimedia_api') . $value;
	}
		
		
	/**
	* Path MULTIMEDIA IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function multimedia_api_thumbs($value = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('multimedia_api_thumbs') . $value;
	}
		
	/**
	* Path MULTIMEDIA IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function facility_page($value = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('facility_page') . $value;
	}

	/**
	* Path MULTIMEDIA IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function field_page($facility = NULL, $field = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('field_page') . $facility . '/' . $field;
	}
		

	/**
	* Path MULTIMEDIA IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function league_page($value = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('league_page') . $value;
	}
		
			/**
	* Path MULTIMEDIA IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function game_page($value = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('game_page') . $value;
	}
		

	/**
	* Path MULTIMEDIA IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function team_page($value = NULL)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('team_page') . $value;
	}
	
													
	/**
	* Path IMAGES
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function path_images()
	{
	    $CI =& get_instance();
	    return base_url().$CI->config->slash_item('image_path');
	}
	
	// ------------------------------------------------------------------------
	
	/**
	* Path CSS
	*
	* Creates http://www.domain.com/css
	* or whatever the css file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function path_css($file)
	{
	    $CI =& get_instance();
	    return base_url().$CI->config->slash_item('css_path') . $file;
	}
	
	// ------------------------------------------------------------------------
	
	/**
	* Path JS
	*
	* Creates http://www.domain.com/js
	* or whatever the js file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function path_js()
	{
	    $CI =& get_instance();
	    return base_url().$CI->config->slash_item('js_path');
	}
	
	// ------------------------------------------------------------------------
	
	/**
	* Path FLASH
	*
	* Creates http://www.domain.com/flash
	* or whatever the flash file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function path_flash()
	{
	    $CI =& get_instance();
	    return base_url().$CI->config->slash_item('flash_path');
	}
	
	// ------------------------------------------------------------------------


	/**
	* Path MULTIMEDIA IMAGES FROM API
	*
	* Creates http://www.domain.com/images
	* or whatever the image file path that is specified in config
	*
	* @access    public
	* @param    string
	* @return    string
	*/
	function wowza_dvr($field, $stream)
	{
	    $CI =& get_instance();
	    return $CI->config->slash_item('wowza_url') . $field . '/' . $stream . '/playlist.m3u8?DVR';
	}
		

}
