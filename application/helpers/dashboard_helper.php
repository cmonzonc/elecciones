<?php 

function load_dashboard($role, $team, $userid, $prev_url = NULL){

	$url = '';
	switch($role){
		case SUPERADMIN:
			$url = base_url() . "superadmin/view_game_schedule";
			break;
		case ADMIN:
			$url = base_url() . "dashboard/team/" . $team ;
			break;
		case USER:
			$url = base_url() . "dashboard/profile/" . $userid;
			break;
		case COACH:
			$url = base_url() . "dashboard/profile/". $team;
			break;
		case FREE:
			$url = base_url() . "admin/view_requests_pending";
			break;
		case SCOUT:
			$url = base_url() . "dashboard/team/" . $team ;
			break;
		case CLUB_ADMIN:
			$url = base_url() . "admin/staff";		
			break;
		case FACILITY_MANAGER:
			$url = base_url() . "dashboard/field";
			break;
		case LEAGUE_MANAGER:
			$url = base_url() . "dashboard/schedule";
			break;
		case ARENA_OWNER:
			$url = base_url() . "dashboard/field";
			break;
		case PARENT:
			$url = base_url() . "dashboard/profile/" . $userid;
			break;
		case MARKETING_PARTNER:
			$url = base_url() . "users/add_account_info";
			break;	
		case NULL:
			$url = $prev_url;
			break;
	}
	
	return $url;
}							

function friendly_date($date){

	echo strftime("%b %d, %Y",strtotime($date));
	
}

function set_timezone(){
	
	$CI =& get_instance();
		
	if(isset($_COOKIE['tz'])){
		$tz = timezone_name_from_abbr('', $_COOKIE['tz'], 2);
		if($tz === false) $tz = timezone_name_from_abbr('', $_COOKIE['tz'], 0);
		$CI->session->set_userdata("tz", $tz);
		$CI->session->set_userdata("tz_offset", $CI->input->cookie("tz"));
	}else{
		$tz = timezone_name_from_abbr('', -5*60*60, 1);
		if($tz === false) $tz = timezone_name_from_abbr('', -5*60*60, 0);
		$CI->session->set_userdata("tz", $tz);	
		$CI->session->set_userdata("tz_offset", 0);		
	}

}

function _convert_time_zone($timeFromDatabase){
	
	$CI =& get_instance();

    return date("Y-m-d H:i:s", (strtotime($timeFromDatabase)+$CI->session->userdata("tz_offset")));
    
    
}

function friendly_short_date($date){

	echo strftime("%m/%d/%Y",strtotime($date));
	
}

function friendly_time($date_begin, $date_end){

	echo strftime("%H:%M",strtotime($date_begin)) . ' - ' .  strftime("%H:%M",strtotime($date_end)) ;
	
}

function friendly_date_r($date){

	return strftime("%b %d, %Y",strtotime($date));
	
}

function friendly_timestamp_r($date){

	return strftime("%b %d, %Y - %H:%M",strtotime($date));
	
}

function friendly_time_r($date_begin, $date_end){

	return strftime("%H:%M",strtotime($date_begin)) . ' - ' .  strftime("%H:%M",strtotime($date_end)) ;
	
}

function friendly_timestamp($date){

	echo strftime("%m/%d/%Y, %H:%M",strtotime($date));
		
}

function friendly_start_date($date){

	echo strftime("%I:%M %p (%B %d, %Y)",strtotime($date));
		
}

function current_day(){
	
	$CI =& get_instance();
	echo strftime("%m/%d/%Y", time()+($CI->session->userdata('tz_offset')));
	
}

function event_calendar($date){
	
	echo strftime("%m/%d/%Y %I:%M %p",strtotime($date));
	
}

function video_thumbnail($id){

	echo "https://dc_images.s3.amazonaws.com/37010/sf-247681-1.png";
// 	echo "https://dc_images.s3.amazonaws.com/37010/sf-" . $id . "-1.png";
	
}

function true_response_ajax($body = NULL){
	
	if(! $body){
		$response = array("response" => TRUE);
		$CI =& get_instance();
		$CI->output->set_content_type('application/json');
		echo json_encode($response);		
	}else{
		$CI =& get_instance();
		$CI->output->set_content_type('application/json');
		echo json_encode($body);				
	}


}

function false_response_ajax(){
	
	$response = array("response" => FALSE);
	$CI =& get_instance();
	$CI->output->set_content_type('application/json');
	echo json_encode($response);

}

function role_name($role){
	
	$name = '';
	switch($role){
		case SUPERADMIN:
			$name = "Superadmin";
			break;
		case ADMIN:
			$name = "Team manager";
			break;
		case COACH:
			$name = "Coach";
			break;
		case USER:
			$name = "Player";
			break;
		case CLUB_ADMIN:
			$name = "Club administrator";
			break;
		case SCOUT:
			$name = "Scout";
			break;
		case FREE:
			$name = "Trial account";
			break;
		case FACILITY_MANAGER:
			$name = "Facility manager";
			break;
		case LEAGUE_MANAGER:
			$name = "League manager";
			break;
		case ARENA_OWNER:
			$name = "Arena owner";
			break;
		case PARENT:
			$name = "Parent";
			break;
		case MARKETING_PARTNER:
			$name = "Marketing partner";
			break;	
		case NULL:
			$name = "";
			break;
	}
	
	return $name;
}


function load_admin($role, $team, $userid){
	
	$url = FALSE;
	switch($role){
		case SUPERADMIN:
			$url = base_url() . "superadmin/create_game";							
			break;
		case ADMIN:
			$url = base_url() . "admin";
			break;
	}
	
	return $url;
}


function load_title_tags($area, $url){

	$parsed_url = explode("/", $url);
	//Create instance to get page information from model	
	$CI =& get_instance();
	$CI->load->model('menu/menu_model','m');
	
	$info_page[0] = new stdClass();
	$info_page[0]->title = "Telemetrio";
	$info_page[0]->description = "Telemetrio";
	$info_page[0]->photo = "https://wizardchan.org/test/src/1shuu4q3.wizardchan.test.png";
	
	switch($area){
		case 'player':
			$top_segment = FALSE;
			if(isset($parsed_url[7])){
				$info_page = $CI->m->tags_player_game_widget($parsed_url[7], $parsed_url[5], $parsed_url[6]); // token, player, game
				$top_segment = TRUE;
			}
			if(isset($parsed_url[6]) AND $top_segment !== TRUE){
				$info_page = $CI->m->tags_player_game($parsed_url[5], $parsed_url[6]); // token, player, game
				$top_segment = TRUE;
			}
			if(isset($parsed_url[5]) AND $top_segment !== TRUE){
				$info_page = $CI->m->tags_player($parsed_url[5]); // token, player, game
				$top_segment = TRUE;
			}
			
			break;
		case 'games':
			$top_segment = FALSE;
			if(isset($parsed_url[6])){
				$info_page = $CI->m->tags_game_widget($parsed_url[6]); // token, player, game
				$top_segment = TRUE;
			}
			if(isset($parsed_url[5]) AND $top_segment !== TRUE){
				$info_page = $CI->m->tags_game($parsed_url[5]); // token, player, game
				$top_segment = TRUE;
			}
			break;
		case 'v':
			$info_page = $CI->m->tags_video($parsed_url[5]); // token, player, game
			break;		
		case 'home':
/* 			$info_page = $CI->m->home_share($parsed_url[5]); // token, player, game */
/* 			return $info_page; */
			break;		
	}
	
	return $info_page;	
	
}

function drop_underlines($text){

	echo str_replace('_', ' ', $text); 
	
}

function random_color(){
	
	$colors = array('1'=>'pink', '2' => 'blue', '3' => 'yellow');
	
	echo $colors[rand(1, 3)];
	
}

function print_target($text){
	
	$target = str_replace(" ", "-", strtolower($text));
	
	echo $target;
	
}
function draw_widgets($widgets, $data, $multicol = 1, $break = NULL){
		
		$CI =& get_instance();
		
		$row = 0;
		$color_row = 1;
		for ($k=0; $k < sizeof($widgets); $k++) {
// 			if($row % 2 == 1){$color_row = 2;}else{$color_row = 1;}
			$CI->layouts->set_widget_body($widgets[$k]->body);
			$CI->layouts->set_widget_shrink($widgets[$k]->shrink);
			$CI->layouts->set_widget_basis($widgets[$k]->basis_a);
			$CI->layouts->set_widget_width($widgets[$k]->width);
			$CI->layouts->set_widget_grow($widgets[$k]->grow_y);
			$CI->layouts->set_widget_id($widgets[$k]->idWidget);
			$CI->layouts->set_color_row($color_row);
			$CI->layouts->set_helper($widgets[$k]->helper);
			$CI->layouts->set_title_widget($widgets[$k]->name);
			$CI->layouts->enable_title($widgets[$k]->title);
			
			// Show layout in two columns
			if($multicol == 2){
				if($k == 0){
					$CI->layouts->set_divider_open('<div class="page-container"><div class="col-md-6 col-sm-12 col-xs-12 p-none-i m-none"><div class="width-full flex flex-direction-r flex-wrap-y">');
					$CI->layouts->set_divider_mid('');
					$CI->layouts->set_divider_close('');
				}elseif($k == $break){
					$CI->layouts->set_divider_mid('</div></div><div class="col-md-6 col-sm-12 col-xs-12 p-none-i m-none"><div class="width-full width-full flex flex-direction-r flex-wrap-y">');
					$CI->layouts->set_divider_open('');
					$CI->layouts->set_divider_close('');
				}elseif($k == sizeof($widgets)-1){
					$CI->layouts->set_divider_close('</div></div></div>');
					$CI->layouts->set_divider_open('');
					$CI->layouts->set_divider_mid('');
				}else{
					$CI->layouts->set_divider_open('');
					$CI->layouts->set_divider_mid('');
					$CI->layouts->set_divider_close('');
				}
			}else{
				if($k == 0){
					$CI->layouts->set_divider_open('<div class="width-full flex flex-direction-r flex-wrap-y">');
					$CI->layouts->set_divider_mid('');
					$CI->layouts->set_divider_close('');
				}elseif($k == sizeof($widgets)-1){
					$CI->layouts->set_divider_close('</div>');
					$CI->layouts->set_divider_open('');
					$CI->layouts->set_divider_mid('');
				}else{
					$CI->layouts->set_divider_open('');
					$CI->layouts->set_divider_mid('');
					$CI->layouts->set_divider_close('');
				}			
			}
			
			$CI->layouts->widgets('widget/' . $widgets[$k]->page, 'dashboards/dashboard3_', $data);				

			$row++;
			
		}
	
}

?>