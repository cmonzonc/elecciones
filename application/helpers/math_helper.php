<?php

function frame_to_time($frame){
	$m_temp 	= $frame/60;
	$minutes 	= floor($m_temp);
	$seconds 	= floor(($m_temp - $minutes)*60);
	
	if($minutes <= 9){
		$minutes = '0' . $minutes;
	}
	if($seconds == 0){
		$seconds = '00';
	}
	if($seconds <= 9 AND $seconds > 0){
		$seconds = '0' . $seconds;		
	}
	return $minutes . "'" . $seconds . "''";
}


function flv_convert_get_thumb($in, $out_thumb)
{
	  // get thumbnail
	  $cmd = '/usr/local/bin/ffmpeg -v 0 -y -i '.$in.' -vframes 1 -ss 5 -vcodec mjpeg -f rawvideo -s 286x160 -aspect 16:9 '.$out_thumb ;
	echo $cmd;
	exec($cmd." 2>&1", $out, $ret);
	if ($ret){
	    echo "There was a problem!\n";
	    print_r($out);
	}else{
	    echo "Everything went better than expected!\n";
	}
	  // $res is the output of the command
	  // transcode video
	 /*
	 $cmd = 'mencoder '.$in.' -o '.$out_vid.' -af volume=10 -aspect 16:9 -of avi -noodml -ovc x264 -x264encopts bitrate=500:level_idc=41:bframes=3:frameref=2: nopsnr: nossim: pass=1: threads=auto -oac mp3lame';
	  $res = shell_exec($cmd);
	*/

}


function plural_($number){
	
	$plural = '';
	if($number > 1){
		$plural = 's';
	}
	
	return $plural;
}

function get_local_time(){
	
	$timeZone = 'America/Detroit';  // +2 hours     	
    $dateSrc = gmdate("Y-m-d H:i:s", time());
    
    $dateTime = new DateTime($dateSrc, new DateTimeZone('GMT')); 
    $dateTime->setTimeZone(new DateTimeZone($timeZone)); 
    
    return $dateTime;
    
}

function change_time($dateSrc){
	
	$timeZone = 'America/Detroit';  // +2 hours     	
    
    $dateTime = new DateTime($dateSrc, new DateTimeZone('GMT')); 
    $dateTime->setTimeZone(new DateTimeZone($timeZone)); 
    
    return $dateTime;	
	
}

function time_ago($date){
	
	$diff = date_diff(change_time($date), get_local_time());

	$plural = 's';
	
	$years 		= $diff->y;	
	if($years == 0){
		$months		= $diff->m;
			if($months == 0){
				$days 		= $diff->d;
					if($days == 0){
						$hours		= $diff->h;
						if($hours == 0){
							$minutes	= $diff->i;																	
							$output = $minutes . ' minute' . plural_($minutes);
						}else{
							$output = $hours . ' hour' . plural_($hours);							
						}
					}else{
						$output = $days . ' day' . plural_($days);			
					}
			}else{
				$output = $months . ' month' . plural_($months);				
			}
	}else{
		$output = $years . ' year' . plural_($years);
	}
	
	return $output . ' ago';

}

function current_url_token($token, $id){
	
	$url = explode("/", $token);
	
	if(isset($url[7])){
		return $token;
	}else{
		$id_ = token_share_($id);
		return $token . '/' . $id_;
	}
	
}

function current_url_token_js($token){
	
	$url = explode("/", $token);
	
	if(isset($url[7])){
		unset($url[7]);
		$token = implode("/", $url);
		echo $token;
	}else{
		echo $token;
	}
	
}


function coupon(){

    $template   = 'XX99X-XX9X9-9X9XX-9X9XX-XXXXX';
    $k = strlen($template);
    $serial_number = '';
    for ($i=0; $i<$k; $i++)
    {
        switch($template[$i])
        {
            case 'X': $serial_number .= chr(rand(65,90)); break;
            case '9': $serial_number .= rand(0,9); break;
            case '-': $serial_number .= '-';  break; 
        }
    }
    return $serial_number;

}


function random(){

	$template   = 'XX99XXX9X99X9XX9X9XXXXXXX';
    $k = strlen($template);
    $serial_number = '';
    for ($i=0; $i<$k; $i++)
    {
        switch($template[$i])
        {
            case 'X': $serial_number .= chr(rand(97,122)); break;
            case '9': $serial_number .= rand(0,9); break;
            case '-': $serial_number .= '-';  break; 
        }
    }
    
    return $serial_number;	

}

function token_share($token){

	if (sizeof($token) === 0) {
		$template   = 'XX99XXX9X99X9XX9X9XXXXXXX';
	    $k = strlen($template);
	    $serial_number = '';
	    for ($i=0; $i<$k; $i++)
	    {
	        switch($template[$i])
	        {
	            case 'X': $serial_number .= chr(rand(97,122)); break;
	            case '9': $serial_number .= rand(0,9); break;
	            case '-': $serial_number .= '-';  break; 
	        }
	    }
	    return $serial_number;	
	}else{
		return $token;
	}	

}

function token_share_($token){

	if (sizeof($token) === 0) {
		$template   = 'XX99XXX9X99X9XX9X9XXXXXXX';
	    $k = strlen($template);
	    $serial_number = '';
	    for ($i=0; $i<$k; $i++)
	    {
	        switch($template[$i])
	        {
	            case 'X': $serial_number .= chr(rand(97,122)); break;
	            case '9': $serial_number .= rand(0,9); break;
	            case '-': $serial_number .= '-';  break; 
	        }
	    }
	    return $serial_number;	
	}else{
		return $token;
	}	

}


function first_shared($token, $first){
	
	if (sizeof($token) === 0 || $first == 1) { return 1;} else { return 0;}
	
}


function time_to_seconds($time){
	
	$portion = explode(':', $time);

	$total = $portion[0]*3600 + $portion[1]*60 + $portion[2]*1;
	
	return $total;
}


function calculate_results($id, $goals, $id_team_session){

	$teams = explode('@', $id);
	$goals = explode('@', $goals);
	
	$i = 0;

	foreach($teams as $team){
		$result[$team]["goals"] = $goals[$i];
		if($team == $id_team_session){
			$result[$team]["active"] = TRUE;
			$result[$team]["idTeam"] = $team;
		}else{
			$result[$team]["active"] = FALSE;
			$result[$team]["idTeam"] = $team;
		}
		$i++;
	}
	
	$head = reset($result);
	$bottom = end($result);
	
	if($head["goals"] > $bottom["goals"]){
		$result[$head["idTeam"]]["winner"] = "WIN";
		$result[$bottom["idTeam"]]["winner"] = "LOSE";
	}
	elseif($head["goals"] < $bottom["goals"]){
		$result[$bottom["idTeam"]]["winner"] = "WIN";
		$result[$head["idTeam"]]["winner"] = "LOSE";
	}else{
		$result[$bottom["idTeam"]]["winner"] = "NULL";
		$result[$head["idTeam"]]["winner"] = "NULL";
	}
	
	// The information of the current team will be always at the top of the array
	if($head["active"] !== TRUE){
		$result_r = array_reverse($result);
	}else{
		$result_r = array_reverse(array_reverse($result));
	}	
	
	switch($result[$id_team_session]["winner"]){
		case "NULL":
			$answer = "T " . $result_r[0]["goals"] . ' - ' . $result_r[1]["goals"];
			break;
		case "WIN":
			$answer = "W " . $result_r[0]["goals"] . ' - ' . $result_r[1]["goals"];
			break;
		case "LOSE":
			$answer = "L " . $result_r[0]["goals"] . ' - ' . $result_r[1]["goals"];
			break;
	}

	echo $answer;
}

?> 
