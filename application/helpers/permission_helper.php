<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
  function verify_permission($permissions_map){

  	if($permissions_map){
	    $CI = get_instance();
	    
		$functionality = $CI->router->fetch_class() . '/'. $CI->router->fetch_method();
        if (in_array($functionality, $permissions_map)){
	        return TRUE;
        }else{

	        if(empty($CI->session->userdata())){
				$CI->session->set_userdata('redirect_game', current_url());
		        redirect('signin');		           
	        }else{
		        if($CI->session->userdata('redirect_game')){
			        $t_url = $CI->session->get_userdata('redirect_game');
			        $CI->session->unset_userdata('redirect_game');
			        redirect($t_url);
		        }else{
					redirect(load_dashboard($CI->session->userdata('role'), $CI->session->userdata('team'), $CI->session->userdata('userid'), current_url()));			        
		        }
	        }
        }     	
        
	}else{
	    redirect('signin');
// 		show_error('You do not have access to this page!');   
	}     	

  }
  
  function user_role($role){
	  
	  return ($role ? $role : FREE);

  }
  

?>