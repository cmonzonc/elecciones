<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
if(!function_exists('active_link')) {
	
  function activate_menu($controller) {
    // Getting CI class instance.
    $CI = get_instance();
    // Getting router class to active.
    $class = '/' . $CI->router->fetch_class() . '/'. $CI->router->fetch_method();

    return ($class == $controller) ? 'active' : '';
  }


  function find_activate_menu($array) {
    // Getting CI class instance.
    $CI = get_instance();
    // Getting router class to active.
    
    foreach($array as $k){
		$test[] = (activate_menu($k->menu_link) == 'active') ? TRUE : FALSE;
    }

	return (count(array_filter($test)) == 1) ? 'active open' : '';

  }
  

  
}

?>