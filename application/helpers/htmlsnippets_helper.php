<?php
/**
 * 
 * 
 */

class Snippets_helper {
	
	static $_membresia = "";

    //Making style by level 
    {
        $CI =& get_instance();
        $CI->load->model('model_parametro');
        $m_parametro = new model_parametro();

        $m_usuario = new model_usuario();
        $usuario = $m_usuario->getAllUserData($CI->session->userdata('IDUSUARIO'));

        $arr_data = $m_parametro->get(array("CODIGO" => $usuario[0]->TIPONIVEL));

        $COLOR_BACKG = $arr_data[0]->COLOR_BACKG;
        $COLOR_TEXT_MM = $arr_data[0]->COLOR_TEXT_MM;
        $COLOR_TEXT_HOVER = $arr_data[0]->COLOR_TEXT_HOVER;
        $COLOR_MM_CURR = $arr_data[0]->COLOR_MM_CURR;
        $COLOR_SUBMENU = $arr_data[0]->COLOR_SUBMENU;
        $COLOR_TEXT_SUBMENU = $arr_data[0]->COLOR_TEXT_SUBMENU;
        $COLOR_BORD_1 = $arr_data[0]->COLOR_BORD_1;
        $COLOR_BORD_2 = $arr_data[0]->COLOR_BORD_2;

        $st = "
                       #main_menu
                        {
                            text-align:center;
                            background: " .$COLOR_BACKG. ";
                            height:40px;
                            font-family: Tw Cen MT Condensed, sans-serif;
                        }
                        #main_menu h1
                        {
                            display:block;
                            padding:12px 0px 0px 0px;
                            margin:0px;
                            color:" .$COLOR_TEXT_MM. ";
                        }
                        #main_menu ul
                        {
                            margin:0;
                            padding:0;
                            list-style:none;
                            overflow:auto;
                        }
                        #main_menu ul li.top_menu
                        {
                            float:left;
                            padding:0px;
                            margin: 7px 5px 0 0;
                        }

                        #main_menu ul li a
                        {
                            font-size:20px;
                            color:" .$COLOR_TEXT_MM. ";
                            display:block;
                            padding:6px 10px 4px 10px;
                        }
                        #main_menu ul li a:hover,
                        .selected_menu,
                        .current_menu
                        {
                            background:" .$COLOR_MM_CURR. ";
                            color:" .$COLOR_BACKG. " !important;

                            /*box-shadow:2px 2px 5px #999;
                             -moz-border-radius:6px;
                             -webkit-border-radius:6px;*/
                            text-decoration:none;
                        }
                        #main_menu ul li a.topmenu_item:hover,
                        .selected_menu
                        {
                            background:" .$COLOR_BACKG. " !important;
                            /*-moz-border-radius:6px 6px 0px 0px;
                             -webkit-border-radius:6px 6px 0px 0px;*/
                        }
                        #main_menu ul ul
                        {
                            display:none;
                            position:absolute;
                            background:" .$COLOR_MM_CURR. ";
                            padding:0px;
                            z-index: 5;
                        }
                        /*#main_menu ul ul li{    margin-top:-1px;}*/
                        #main_menu ul ul a
                        {
                            color:" .$COLOR_TEXT_SUBMENU. ";
                            font-size: 20px;
                            padding:7px 20px 7px 8px;

                            border-top: 1px solid " .$COLOR_MM_CURR.";
                            border-bottom: 1px solid " .$COLOR_BORD_1.";
                        }
                        #main_menu ul ul a:hover,
                        #main_menu ul ul a.selected
                        {
                            background:#586901;
                            color:" .$COLOR_BACKG. " !important;
                            border-bottom: 1px solid " .$COLOR_BORD_1.";
                            border-top: 1px solid " .$COLOR_BORD_2.";
                        }
        ";
        return $st;
    }


	static function get_footer_admin($_section = NULL, $_use_wrapper = true)
	{
		$html = '';
		if($_use_wrapper){
			$html .= "\t\n</div>\n";
			$html .= '</div><div id="footer"><div class="wrapper_content">';
		}
		if($_use_wrapper){
			$html .= '</div></div>';
		}
		$html .= "</body>\n</html>";
		return $html;
	}

	/**
	 * showMensage() 
	 * @static
	 * @param string $_wrapper
	 * @param string $_clase
	 * @return bool|string
	 */

	static function show_message($_wrapper='p', $_clase = 'lista_msg')
	{
		$CI =& get_instance();
		$return = '';
		if($CI->session->flashdata('msg')){
			$mensaje = $CI->session->flashdata('msg');
			$return .= '<'.$_wrapper.' class="msg_'.$mensaje['tipo'].' '.$_clase.'">';
			$return .= $mensaje['msg'];
			$return .= '</'.$_wrapper.'>';
		}else{
			return false;
		}
		return $return;
	}

	static function output($_data)
	{
		return '<pre>'. print_r($_data, true).'</pre>';
	}


	/*---------------------- Input forms ------------------------*/
	
	static function select($_id, $_label,Array $_data,Array $_cols, $_selected=false, $_options='')
	{
		$select = '<select id="'.$_id.'" name="'.$_id.'" '.$_options.' class="input">';
		$select .='<option value="">'.$_label.'</option>';
		foreach($_data as $v){
			$sel = '';
			if($v->$_cols['id'] == $_selected){
				$sel = 'selected="selected"';
			}
			$select .='<option value="'.$v->$_cols['id'].'" '.$sel.'>'.$v->$_cols['nombre'].'</option>';
		}
		$select .= '</select>';
		return $select;
	}

	static function button($_id, $_label, $_classicon='')
	{
		return '<button class="button" type="submit" id="'.$_id.'"><span class="'.$_classicon.'"></span>'.$_label.'</button>';
	}

	static function input_text($_id, $_value='', $_options='')
	{
		return '<input type="text" id="'.$_id.'" name="'.$_id.'" value="'.$_value.'" class="input" '.$_options.' />';
	}

	static function input_hidden($_id, $_value='', $_options='')
	{
		return '<input type="hidden" id="'.$_id.'" name="'.$_id.'" value="'.$_value.'" class="input" '.$_options.' />';
	}

	static function input_password($_id, $_value='', $_options='')
	{
		return '<input type="password" id="'.$_id.'" name="'.$_id.'" value="'.$_value.'" class="input" '.$_options.' />';
	}
	static function checkbox($_id, $_label, $_selected=false, $_options='')
	{
		$_check = '';
		if($_selected) $_check = 'checked="checked"';
		return '<label for="'.$_id.'"><input type="checkbox" '. $_check .' id="'.$_id.'" name="'.$_id.'" '.$_selected.' '.$_options.' /> '.$_label.'</label>';
	}

}

?>