<?php


function get_names_from_cache($cache, $id_game){

	$order_games = $cache['order_games'][$id_game];
	$teams = '';
	foreach($order_games as $team){
		$teams[] = $team->name;
	}

	return $teams[0] . ' VS ' . $teams[1];
}

?>