<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
if (! function_exists('build_grid_js'))
{

	function build_grid_js($grid_id, $url, $colModel, $sortname = NULL, $sortorder = 'asc', $gridParams = NULL, $buttons = NULL)
	{
		//Basic propreties
		$grid_js = '$("#'.$grid_id.'").flexigrid({';
		$grid_js .= "url: '".$url."',";
		$grid_js .= "dataType: 'json',";
		$grid_js .= "sortname: '". $sortname ."',";
		$grid_js .= "sortorder: '".$sortorder."',";
		$grid_js .= "usepager: true,";
		$grid_js .= "useRp: true,";
		
		//Other propreties
		if($gridParams == NULL)
		{
			//Default params
			$gridParams = array(
				'width' => 'auto',
				'height' => 'auto',
				'rp' => 15,
				'rpOptions' => '[10,15,20,100]',
				'pagestat' => 'Mostrando: {from} a {to} de {total} registros.',
				'blockOpacity' => 1
				//'title' => 'Lista de Imagenes',
				//'showTableToggleBtn' => true
			);
		}

		if (is_array($gridParams))
		{
			//String exceptions that dont have ' '. Must be lower case.
			$string_exceptions = array("rpoptions");
			
			//Print propreties
			foreach ($gridParams as $index => $value) {
				//Check and print with or without ' '
				if (is_numeric($value)) {
					$grid_js .= $index.": ".$value.",";
				} 
				else 
				{
					if (is_bool($value))
						if ($value == true)
							$grid_js .= $index.": true,";
						else
							$grid_js .= $index.": false,";
					else
						if (in_array(strtolower($index),$string_exceptions))
							$grid_js .= $index.": ".$value.",";
						else
							$grid_js .= $index.": '".$value."',";
				}
			}
		}
		
		$grid_js .= "colModel : [";
		
		//Get colModel
		foreach ($colModel as $index => $value) {
			$grid_js .= "{display: '".$value[0]."', ".($value[2] ? "name : '".$index."', sortable: true," : "")." width : ".$value[1].", align: '".$value[3]."'".(isset($value[5]) && $value[5] ? ", hide : true" : "")."},";  
			
			//If item is searchable
			if ($value[4] != 0)
			{
				//Start searchitems var
				if (!isset($searchitems))
					$searchitems = "searchitems : [";
					
				if ($value[4] == 2)
					$searchitems .= "{display: '".$value[0]."', name : '".$index."', isdefault: true},";
				else if ($value[4] == 1)
					$searchitems .= "{display: '".$value[0]."', name : '".$index."'},";
			}
				
		}
		//Remove the last ","
		$grid_js = substr($grid_js,0,-1).'],';
		$searchitems = substr($searchitems,0,-1).']';
		
		//Add searchitems to grid
		$grid_js .= $searchitems;

		//Get buttons
		if (is_array($buttons)) 
		{
			$grid_js .= ",buttons : [";
            //print_r($buttons);
           // exit;
			foreach ($buttons as $index => $value) {
				if ($value[0] == 'separator')
					$grid_js .= "{separator: true},";
				else
					$grid_js .= "{name: '".$value[0]."', bclass : '".$value[1]."', onpress : ".$value[2]."},";
					//$grid_js .= "{name: '".$value[0]."', bclass : '".$value[1]."'},";
			}
			//Remove the last ","
			$grid_js = substr($grid_js,0,-1).']';
		} 
		
		//Finalize
		$grid_js .= "});";
		
		return $grid_js;
	}
}

?>