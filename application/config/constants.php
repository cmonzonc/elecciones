<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', FALSE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



/*
|--------------------------------------------------------------------------
| Subscriptions  
|--------------------------------------------------------------------------
|
|
*/

define("SUPERADMIN", "1");
define("ADMIN", 	"2");
define("USER", 		"3");
define("COACH", 	"4");
define("FREE", 		"5");
define("SCOUT",		"6");	
define("CLUB_ADMIN", "7");
define("FACILITY_MANAGER", "8");
define("LEAGUE_MANAGER", "9");
define("ARENA_OWNER", "10");
define("PARENT", "11");
define("MARKETING_PARTNER", "12");

/*
|--------------------------------------------------------------------------
| Claims  
|--------------------------------------------------------------------------
|
|
*/

define("QUEUE",  	"1");
define("APPROVE", 	"2");
define("DENY",		"3");
define("RESEND", 	"4");	
define("DELETE", 	"5");	
define("REMOVE", 	"6");	

/*
|--------------------------------------------------------------------------
| Error messages 
|--------------------------------------------------------------------------
|
|
*/

define("DENY_TITLE", 			"Sorry, you don't have access to this area");
define("DENY_BODY", 			"You don't have the necessary credential to access");
define("NOT_EXIST_TITLE", 		"Sorry, this page isn't available");
define("NOT_EXIST_BODY", 		"The link you followed may be broken, or the page may have been removed.");	
define("PLAYER_EMPTY", 			"Not exists player registered in this team.");	
define("GAME_PRESENCE", 		"The current player is not registered in this game");	
define("MESSAGE_REQUEST_SENT",	"Request sent");	
define("SUBSCRIPTION_COMPLETE",	"Subscription complete, enter your credentials");	
define("UNKNOWN PLAYER",		"Unidentified Player");	
define("TEAM_PERMISSIONS_DENY",	"You don't have enough permission to register information in the team");	
define("SUCCESFUL_STAFF_REGISTRATION",	"The staff was successful created");	
define("INVALID_VALUES",		"One or more of the identifiers do not exists");	
define("ALREADY_REGISTERED",			"2");	
define("ALREADY_REGISTERED_MESSAGE",	"Member is already registered");	
define("RECORD_NOT_FOUND",				"3");	
define("RECORD_NOT_FOUND_MESSAGE",		"Member is not already registered in the staff");
define("HELP_INFORMATION_DRAG_DROP",	"Select team and drag and drop a member from the left column to the right column to register on staff");
define("MAX_TIME_LIMIT_ERROR",			"Please verify dates and time, the maximum time allowed is two hours.");
define("TIME_BEGIN_PAST_ERROR",			"The game cannot be registered. Please verify the start date, you only can register games with a start date and time in the future. Consider that should be 5 minutes in the future.");
define("OVERLAP_TIME_ERROR_G",			"Please, change the time and date. The time is overlapped with the next game(s)");
define("OVERLAP_TIME_ERROR_T",			"Please, change the time and date. The time is overlapped with the next training(s)");
define("END_BEFORE_START_ERROR",		"Please, verify date and time. Ends before start.");
define("TIME_RECORDING_ERROR",			"Minimum recording time is 5 minutes");
/* End of file constants.php */
/* Location: ./application/config/constants.php */


/*
|--------------------------------------------------------------------------
| Like actions 
|--------------------------------------------------------------------------
|
|
*/


define("LIKE_PLAYER_GAME", 		"1");
define("DISLIKE_PLAYER_GAME", 	"2");
define("LIKE_GAME", 			"3");
define("DISLIKE_GAME", 			"4");
define("LIKE_TEAM", 			"5");
define("DISLIKE_TEAM",		 	"6");
define("LIKE_PLAYER", 			"7");
define("DISLIKE_PLAYER", 		"8");
define("LIKE_VIDEO_GAME", 		"9");
define("DISLIKE_VIDEO_GAME", 	"10");


define("LIKE_ERROR_MESSAGE", 			"0");

define("LIKE_PLAYER_GAME_MESSAGE", 		"1");
define("DISLIKE_PLAYER_GAME_MESSAGE", 	"2");
define("LIKE_GAME_MESSAGE", 			"3");
define("DISLIKE_GAME_MESSAGE", 			"4");
define("LIKE_TEAM_MESSAGE", 			"5");
define("DISLIKE_TEAM_MESSAGE",		 	"6");
define("LIKE_PLAYER_MESSAGE", 			"7");
define("DISLIKE_PLAYER_MESSAGE", 		"8");

define("LIKED", 		"Added to Liked widgets");
define("DISLIKED", 		"Removed from Liked widgets");
define("DISLIKED_ADD", 	"Added to Disliked widgets");
define("DISLIKED_REMOVE","Remove from Disliked widgets");
define("SWAP_LIKE",		"Swap like");
define("EXPIRE_SUBSCRIPTION", "Your subscription has expired");

/*
|--------------------------------------------------------------------------
| Message join 
|--------------------------------------------------------------------------
|
|
*/
define("TERMS", "Terms of service");

/* define("", ""); */
define("TEAM_CHOICE", 			"Choose your team and the role you play in the selected team");
define("VERIFICATION_MESSAGE", 	"A request will be sent to the administrator, your personal information will be checked and verified that you are a coach or team owner of the selected team, while the process is in queue you can only see the public profiles.");


/*
|--------------------------------------------------------------------------
| Video type
|--------------------------------------------------------------------------
|
*/
define("HIGHLIGHT", 	"0");
define("GOAL",			"1");
define("HALF", 			"2");
define("HALF_TWO", 		"3");

/*
|--------------------------------------------------------------------------
| Game state
|--------------------------------------------------------------------------
|
*/
define("GAME_QUEUE", 	 "0");
define("GAME_PLAYING",	 "2");
define("GAME_PLAYED",	 "3");
define("GAME_PROCESSED", "1");

/*
|--------------------------------------------------------------------------
| Bookmarker
|--------------------------------------------------------------------------
|
*/
define("BOOK_STATE_START", 	"0");
define("BOOK_STATE_END",	"1");
define("BOOK_GREAT",	 	"2");
define("BOOK_GOAL",		 	"3");
define("BOOK_IMPROVE", 		"4");


/*
|--------------------------------------------------------------------------
| Game state
|--------------------------------------------------------------------------
|
*/

define("TIME_PLAYED_COUNTER", "20");
define("GOOGLE_API_KEY", "AIzaSyDwHMdDezNQMF500XaXiThV6efLCuQr198"); // Place your Google API Key


/*
|--------------------------------------------------------------------------
| Interface
|--------------------------------------------------------------------------
|
*/

define("NO_COACH_REGISTERED", 	"No coach registered in the teams");
define("NO_GAME_REGISTERED",	"There are no games");
define("NO_MULTIMEDIA_AVAILABLE",	"No multimedia content available");
define("UPLOAD_TITLE",			"Upload file");


/*
|--------------------------------------------------------------------------
| Addresses
|--------------------------------------------------------------------------
|
*/

define("PHOTO_TEAM", 	"/assets/uploads/team/");


/*
|--------------------------------------------------------------------------
| Tables
|--------------------------------------------------------------------------
|
*/

define("ITEMS_PER_PAGE",	10);
define("BID_DACAST", 		"37010");
define("API_KEY_DACAST",	"5aed3655789dba7e9f2b");


/*
|--------------------------------------------------------------------------
| Time zone
|--------------------------------------------------------------------------
|
| These time zones are used for MySQL
|
*/


define('TIME_ZONE', 'America/Detroit');

/*
|--------------------------------------------------------------------------
| Layouts
|--------------------------------------------------------------------------
|
*/

define("SHRINK_YES",	'flex-shrink-y');
define("SHRINK_NO",		'flex-shrink-n');
define("GROW_Y", 		'flex-grow-y');
define("BASIS",			'flex-basis-a');
define("WIDTH",			'width-full');
define("BODY_SCROLLABLE",'panel-body p-m height-max-400 js-scroll overflow-y-a');
define("BODY_SIMPLE",	'panel-body p-m');
define("EMPTY",			'');