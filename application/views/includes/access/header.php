<!DOCTYPE html>
<html lang="en">
	<!-- HEADER -->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>Stadio<?php echo $title_for_layout; ?></title>

		<?php echo $this->layouts->print_includes(); ?>

		<link rel="icon" href="/favicon.jpeg" type="image/jpeg">
		<link href="/favicon.jpeg" rel="apple-touch-icon" type="image/jpeg">

		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>
	<!-- END HEADER -->

	<body class="login">
		
	<script type="text/javascript">
	get_time_zone_offset();
	function get_time_zone_offset() {		
	    var c_d = new Date( );
	    var g_o_s = c_d.getTimezoneOffset()*-60;
	    createCookie("tz", g_o_s, 30);
	    return g_o_s;
	}
	function createCookie(name,value,days) {	
	    if (days) {
	        var date = new Date();
	        date.setTime(date.getTime()+(days*24*60*60*1000));
	        var expires = "; expires="+date.toGMTString();
	    }
	    else var expires = "";
	    document.cookie = name+"="+value+expires+"; path=/";
	}	
	</script>


