<!DOCTYPE html>
<html lang="en">
	<!-- HEADER -->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>Stadio<?php echo $title_for_layout; ?></title>

		<?php echo $this->layouts->print_includes(); ?>

		<link rel="icon" href="/favicon.jpeg" type="image/jpeg">
		<link href="/favicon.jpeg" rel="apple-touch-icon" type="image/jpeg">

		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>
	<!-- END HEADER -->

	<body class="<?php echo $body_class; ?>">

	<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-use-bootstrap-modal="true" style="display: none;">
		<!-- The container for the modal slides -->
		<div class="slides" style="width: 9636px;"></div>
		<!-- Controls for the borderless lightbox -->
		<h3 class="title"></h3>
		<a class="prev" style="text-decoration: none;">‹</a>
		<a class="next" style="text-decoration: none;">›</a>
		<a class="description" style="text-decoration: none;">›</a>
		<a class="close" style="font-weight: 300; font-family: Helvetica Neue; font-variant: small-caps; transform: scale(1, .95);">×</a>
		<!-- The modal dialog, which will be used to wrap the lightbox content -->
		<div class="modal fade in">
			<div class="modal-dialog" style="width: 100%;">
				<div class="modal-content p-m">
					<div class="modal-body" style="background: black;"></div>
					<div class="modal-footer p-m">
						<div class="row">
							<div class="col-xs-6 align-tx-left">																		
								<div class="display-t">
									<div class="display-tc vertical-align-t p-r-s">
										<i class="fa fa-calendar fa-pull-left color-tx-blue"></i>
									</div>
									<div class="display-tc vertical-align-t" style="min-width: 100px;">
										<h4 class="title m-none color-tx-blue"><a href="<?php echo game_page('16182'); ?>"></a></h4>
										<h5 class="location color-tx-blue m-none"><a href="<?php echo facility_page('1'); ?>"></a></h5>
										<h6 class="date m-none"></h6>
									</div>
								</div>
							</div>
							<div class="col-xs-6 align-tx-right align-el-right display-t">
								<h6 class="user-captured  m-none display-tc align-el-right color-tx-blue p-l-xs"></h6>
									<div class="align-el-right display-tc no-borders" style="margin-top: -5px;">
										<img src="/favicon.jpeg" class="mCS_img_loaded" alt="user" height="15">
									</div>
								<h6 class="m-none display-tc align-el-right p-r-xs">Captured by </h6>
							</div>
						</div>
					<br>
					</div>				
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
	get_time_zone_offset();
	function get_time_zone_offset() {		
	    var c_d = new Date( );
	    var g_o_s = c_d.getTimezoneOffset()*-60;
	    createCookie("tz", g_o_s, 30);
	    return g_o_s;
	}
	function createCookie(name,value,days) {	
	    if (days) {
	        var date = new Date();
	        date.setTime(date.getTime()+(days*24*60*60*1000));
	        var expires = "; expires="+date.toGMTString();
	    }
	    else var expires = "";
	    document.cookie = name+"="+value+expires+"; path=/";
	}	
	</script>


