<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">Copyright © Telemetrio 2016, all rights reserved.
    </div>
    <div class="scroll-to-top" style="display: block;">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!--
	    <a class="anchor-inverse anchor-uppercase m-r-l display-b--sm m-r-none--sm m-b-s--sm" href="#" data-toggle="modal" data-target="#show-terms" id="show-term">Terms of service</a>
	    <a class="anchor-inverse anchor-uppercase m-r-l display-b--sm m-r-none--sm m-b-s--sm" href="#">About</a>
		<a class="anchor-inverse anchor-uppercase display-b--sm" href="#">Contact us</a>
-->
