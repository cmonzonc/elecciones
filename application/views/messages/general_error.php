<?php if(@$error): ?>
<div class="alerts tx-size-s">    
    <div class="note note-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button> <?php echo $error; ?>
    </div>
</div>
<?php endif; ?>

<?php if(@$message): ?>
<div class="alerts tx-size-s">    
    <div class="note note-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button> <?php echo $message; ?>
    </div>
</div>
<?php endif; ?>

<?php if(@$success): ?>
<div class="alerts tx-size-s">    
    <div class="note note-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button> <?php echo $success; ?>
    </div>
</div>
<?php endif; ?>

<?php if(@$info): ?>
<div class="alerts tx-size-s">    
    <div class="note note-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button> <?php echo $info; ?>
    </div>
</div>	    
<?php endif; ?>
      