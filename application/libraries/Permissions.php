<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
    
	/**
	* User management class
	*/
	class Permissions {

	    // init vars
	    var $CI;                        // CI instance
	    var $where = array();
	    var $set = array();
	    var $required = array();
	    	
		function get_general_permissions($idRole){

			$this->CI = & get_instance();

			$this->CI->db->select('category, subcategory');
			$this->CI->db->from('permissions');
			$this->CI->db->join('permission_map', 'permission_map.idPermission = permissions.idPermission');
			$this->CI->db->where('idRole', $idRole);

			$query = $this->CI->db->get();

			if($query->result()){
				foreach($query->result_array() as $r){
					$permissions[] = $r['category'] . '/' . $r['subcategory'];
				}
				return $permissions;
			}else{
				return FALSE;
			}
			
		}
		
		function get_permissions($idRole = ''){
			
			$this->CI->db->select('DISTINCT(category)');
			if($idRole){
				$this->CI->db->where_in('key', $this->get_general_permissions($idRole));
			}
			$this->CI->db->order_by('category');
			$query = $this->CI->db->get('permissions');
			
			if($query->result()){
				$result = $query->result_array();
				foreach($result as $row){
					if($category_permissions = $this->get_permissions_from_category($row['category'])){
						$permissions[$row['category']] = $category_permissions;
					}else{
						$permissions[$row['category']] = '';
					}
					return $permissions;
				}
			}else{
				return FALSE;
			}
			
		}
		
		function get_permissions_from_category($category = ''){
			
			if($category){
				$this->CI->db->where('category', $category);
			}
			$query = $this->CI->db->get('permissions');
			
			if($query->result()){
				return $query->result_array();
			}else{
				return FALSE;
			}
			
		}
		
		function get_permissions_from_subcategory(){
			
			
		}
		
		// Get the permission identifiers from a user role
		function get_permission_index($idRole){
			
			$this->CI->db->select('idPermission');
			$this->CI->db->where('idRole', $idRole);
			$query = $this->CI->db->get('permission_map');
			
			if($query->result()){
				return $query->result_array();
			}else{
				return FALSE;
			}
		}
		
		// Get user roles from database
		function get_roles(){
			
			$query = $this->CI->db->get('roles');
			if($query->result()){
				return $query->result_array();
			}else{
				return FALSE;
			}
		}
		
		// Add permission to a user profile (rol)
		function add_permisssion_to_role($idRole){
			
			$this->CI->db->where('idRole', $idRole);
			$this->CI->db->delete('permission_map');
			
			$post = $this->CI->easysite->get_post();
			foreach($post as $k => $permission){
				if(preg_match('/^perm([0-9]+)/i', $key, $matches)){
					$this->CI->db->set('idRole', $idRole);
					$this->CI->db->set('idPermission', $matches[1]);
					$this->CI->db->insert('permission_map');
				}
			}
			
		}
		
		function add_role($roleName = ''){
			
			if($roleName){
				$this->CI->db->set('name', $roleName);
				$this->CI->db->insert('roles');
				return $this->CI->db->insert_id();
			}else{
				return FALSE;
			}
			
		}
	
	}
	