<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Layouts Class. PHP5 only.
 *
 */
class Layouts

{
	// Will hold a CodeIgniter instance
	private $CI;
	// Will hold a title for the page, NULL by default
	private $title_for_layout = NULL;
	// Will hold a title for the content page, NULL by default
	private $title_for_content = NULL;
	// Will hold a title for the widget, NULL by default
	private $title_for_widget = NULL;
	// Will set if the sidebar will show something
	private $content_for_sidebar = TRUE;
	private $shortcut_url 	= NULL;
	private $shortcut_name 	= NULL;
	// The title separator, ' | ' by default
	private $access_header = NULL;
	// Will hold a title for the widget, NULL by default
	private $title_separator = ' | ';
	private $includes = array();
	
	
	public function __construct(){
		
		$this->CI = & get_instance();
		// General styles and javascript for the website (header)
		$this->add_include('assets/css/style.css')
			 ->add_include('https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all', FALSE)
			 ->add_include('assetsm/global/plugins/font-awesome/css/font-awesome.min.css')
			 ->add_include('assetsm/global/plugins/simple-line-icons/simple-line-icons.min.css')
			 ->add_include('assetsm/global/plugins/bootstrap/css/bootstrap.min.css')
			 ->add_include('assetsm/global/plugins/uniform/css/uniform.default.css')
			 ->add_include('assetsm/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')
			 ->add_include('assetsm/global/plugins/morris/morris.css')
			 ->add_include('assetsm/global/plugins/fullcalendar/fullcalendar.min.css')
			 ->add_include('assetsm/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')
			 ->add_include('assetsm/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')
			 ->add_include('assetsm/global/plugins/jqvmap/jqvmap/jqvmap.css')
			 ->add_include('assetsm/global/css/components.css', TRUE, 'style_components')
			 ->add_include('assetsm/global/css/plugins.min.css')
			 ->add_include('assetsm/layouts/layout/css/layout.css')
			 ->add_include('assetsm/layouts/layout/css/themes/grey.min.css', TRUE, 'style_color')
			 ->add_include('assetsm/layouts/layout/css/custom.min.css');

/*
		$this->add_include('assets/css/style.css')
			 ->add_include('assets/css/font-awesome.min.css')
			 ->add_include('assets/css/bootstrap-datepicker.css')
			 ->add_include('assets/vendor/bootstrapValidator/css/bootstrapValidator.min.css');
*/

		// General styles and javascript for the website (footer)
		$this->add_include_footer('assetsm/global/plugins/jquery.min.js')
			 ->add_include_footer('assetsm/global/plugins/bootstrap/js/bootstrap.min.js')
			 ->add_include_footer('assets/js/app/app.js')
			 ->add_include_footer('assetsm/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')
			 ->add_include_footer('assetsm/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')
			 ->add_include_footer('assetsm/global/plugins/jquery.blockui.min.js')
			 ->add_include_footer('assetsm/global/plugins/uniform/jquery.uniform.min.js')
			 ->add_include_footer('assetsm/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')
			 ->add_include_footer('assetsm/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')
			 ->add_include_footer('assetsm/global/plugins/morris/morris.min.js')
			 ->add_include_footer('assets/js/jquery.mCustomScrollbar.concat.min.js')
			 ->add_include_footer('assetsm/global/scripts/app.js')
			 ->add_include_footer('assetsm/pages/scripts/dashboard.min.js')
			 ->add_include_footer('assetsm/layouts/layout/scripts/layout.min.js')
			 ->add_include_footer('assetsm/layouts/layout/scripts/demo.js')
			 ->add_include_footer('assetsm/layouts/global/scripts/quick-sidebar.min.js')
			 ->add_include_footer('assetsm/global/plugins/moment.min.js')
			 ->add_include_footer('assetsm/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')
			 ->add_include_footer('assetsm/global/plugins/morris/raphael-min.js')
			 ->add_include_footer('assetsm/pages/scripts/components-date-time-pickers.js');
/*
			 ->add_include_footer('assetsm/global/plugins/jquery-migrate-1.2.1.min.js')
			 ->add_include_footer('assetsm/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js')
*/

/*
		$this->add_include_footer('assets/js/jquery.min.js')
			 ->add_include_footer('assets/js/bootstrap.min.js')
			 ->add_include_footer('assets/js/app/validation.js')
			 ->add_include_footer('assets/js/app/app.js')
			 ->add_include_footer('assets/js/bootstrap-datepicker.js')
 			 ->add_include_footer('assets/vendor/bootstrapValidator/js/bootstrapValidator.min.js')
 			 ->add_include_footer('assets/js/jquery.mCustomScrollbar.concat.min.js');
*/

	}
	
	public function set_title($title){
		$this->title_for_layout = $title;
	}

	public function set_content_sidebar($bool){
		$this->content_for_sidebar = $bool;
	}
	
	public function set_helper($helper){
		$this->helper_for_widget = $helper;
	}
	
	public function set_divider_open($tag){
		$this->divider_open = $tag;
	}

	public function set_divider_mid($tag){
		$this->divider_mid = $tag;
	}
	
	public function set_divider_close($tag){
		$this->divider_close = $tag;
	}

	public function set_color_row($color){
		$this->color_for_row = $color;
	}
			
	public function set_title_widget($title){
		$this->title_for_widget = $title;
	}

	public function set_messages($messages){
		$this->messages_for_page = $messages;
	}
	
	public function set_widget_shrink($shrink){

		if($shrink == 1){
			$this->shrink_for_widget = SHRINK_YES;				
		}else{
			$this->shrink_for_widget = SHRINK_NO;
		}

	}

	public function enable_title($title){
		
		if($title == 1){
			$this->enable_title = TRUE;				
		}else{
			$this->enable_title = FALSE;
		}

	}

	public function set_widget_id($id){
		
		$this->widget_id = $id;
		
	}
	
	public function set_widget_body($body){
		
		if($body == 1){
			$this->body_for_widget = BODY_SCROLLABLE;				
		}
		elseif($body == 2){
			$this->body_for_widget = BODY_SIMPLE;
		}else{
			$this->body_for_widget = '';
		}
	}
	
	public function set_widget_basis($basis_a){
		
		if($basis_a == 1){
			$this->basis_for_widget = BASIS;				
		}else{
			$this->basis_for_widget = '';
		}
	}
	
	public function set_widget_width($width){
		
		if($width == 1){
			$this->width_for_widget = WIDTH;
		}else{
			$this->width_for_widget = '';			
		}
	}
	
	public function set_widget_grow($grow_y){

		if($grow_y == 1){
			$this->grow_y_for_widget = GROW_Y;	
		}else{
			$this->grow_y_for_widget = '';				
		}
		
	}

	public function set_title_widget_twice($title1, $title2 = NULL){
		$this->title_for_widget_twice_1 = $title1;
		if($title2){
			$this->title_for_widget_twice_2 = $title2;			
		}

	}

	public function set_shortcut($url, $name){

		$this->shortcut_url = $url;			
		$this->shortcut_name = $name;
	}
		
	// template, $params
	public function view($layout = 'default', $params = array()){
		
		$view_name = $params['main_content'];

		// Handle the site's title. If NULL, don't add anything. If not, add a
		// separator and append the title.
		if ($this->title_for_layout !== NULL) {
			$separated_title_for_layout = $this->title_separator . $this->title_for_layout;
			$title_for_content = $this->title_for_layout;
		}
		
		if($layout == 'page_access'){			
			$header['shortcut_header'] = FALSE;
			$header['title_for_content'] = $this->title_for_layout;			
			if(isset($this->shortcut_url)){
				$header['shortcut_header'] = TRUE;
				$header['shortcut_url'] = $this->shortcut_url;
				$header['shortcut_name'] = $this->shortcut_name;
			}
			$this->access_header = $this->CI->load->view('content/page_access_header', $header, TRUE);
		}
		
		if(isset($this->messages_for_page)){
			if ($this->messages_for_page !== NULL) {
	
				$messages['error'] = @$this->messages_for_page['error'];
				$messages['message'] = @$this->messages_for_page['message'];
				$messages['success'] = @$this->messages_for_page['success'];
				$messages['info'] = @$this->messages_for_page['info'];
				
				$m_e = $this->CI->load->view('messages/general_error', $messages, TRUE);			
			}			
		}else{
			$m_e = '';
		}

		if ($this->content_for_sidebar) {		
	    	$sql = "SELECT * FROM menu_sidebar INNER JOIN menu_sidebar_category USING (idSidebarCategory) WHERE enable = 1 ORDER BY idSidebarCategory ASC";
	     	$query  = $this->CI->db->query($sql);
		 	foreach($query->result() as $item){
			 	$menu[$item->idSidebarCategory][] = $item;
		 	}
		 	$data['content_for_sidebar'] = $menu;
			$content_for_sidebar = $this->CI->load->view('sidebars/sidebar_menu', $data, TRUE);
		}
		// Load the view's content, with the params passed
		$view_content = $this->CI->load->view($view_name, $params, TRUE);

		// Now load the layout, and pass the view we just rendered
		$this->CI->load->view($layout, array(
			'content_for_layout' => $view_content,
			'messages_errors' => $m_e,
			'access_header' => $this->access_header,
			'title_for_layout' => $separated_title_for_layout,
			'title_for_content' => $title_for_content,
			'content_for_sidebar' => $content_for_sidebar
		));
	}
	
	
	public function view_column_stats($view_name, $params){
		
		$view_content = $this->CI->load->view($view_name, $params, TRUE);
		
		return  $view_content;
	}
				
	// template, $params
	public function view_stats($view_name = 'default', $content_array , $layout, $cols = 4){

		for($k = 0; $k < sizeof($content_array); $k++){				
			if($k % ceil(sizeof($content_array)/$cols) == 0){
				$column = array_slice($content_array, $k, ceil(sizeof($content_array)/$cols)); 
				$view_content[] = $this->view_column_stats("widgets/draw_total", array("stats" => $column, "index" => $k)); 		

			}
		}
		
		// Load the view's content, with the params passed

				
		// Now load the layout, and pass the view we just rendered

		$this->CI->load->view($layout, array(
			'content_for_layout' => $view_content
		));

	}
	
	// template, $params
	public function widgets($widget, $layout, $params = array()){
		
		// Handle the site's title. If NULL, don't add anything. If not, add a
		// separator and append the title.
		if ($this->title_for_widget !== NULL) {
			$widget_id 			= $this->widget_id;			
			$shrink 			= $this->shrink_for_widget;
			$basis 				= $this->basis_for_widget;
			$width 				= $this->width_for_widget;
			$grow 				= $this->grow_y_for_widget;
			$title_for_widget 	= $this->title_for_widget;
			$color_for_row 		= $this->color_for_row;
			$helper_for_widget	= $this->helper_for_widget;
			$body_for_widget	= $this->body_for_widget;
			$enable_title 		= $this->enable_title;
			$divider_open 		= $this->divider_open;
			$divider_mid 		= $this->divider_mid;
			$divider_close 		= $this->divider_close;
		}
		// Load the view's content, with the params passed
		$view_content = $this->CI->load->view($widget, $params, TRUE);
		// Now load the layout, and pass the view we just rendered
		$this->CI->load->view($layout, array(
			'widget_id'			=> 'widget-'. $widget_id,
			'width'				=> $width,
			'basis' 			=> $basis,
			'shrink' 			=> $shrink,
			'grow' 				=> $grow,
			'body_for_widget'		=> $body_for_widget,
			'helper_for_widget'		=> $helper_for_widget,
			'color_for_row' 		=> $color_for_row,
			'content_for_widget' 	=> $view_content,
			'title_for_widget' 		=> $title_for_widget,
			'enable_title' 			=> $enable_title,
			'divider_open' 			=> $divider_open,
			'divider_mid' 			=> $divider_mid,
			'divider_close' 		=> $divider_close
		));
	}
	
	// template, $params
	public function widgets_twice($widget1, $widget2 = NULL, $layout, $params = array()){
		
		// Handle the site's title. If NULL, don't add anything. If not, add a
		// separator and append the title.
		if ($this->title_for_widget_twice_1 !== NULL) {
			$color_for_row 				= $this->color_for_row;
			$title_for_widget_twice_1 	= $this->title_for_widget_twice_1;
			if($widget2){
				$title_for_widget_twice_2 	= $this->title_for_widget_twice_2;				
			}else{
				$title_for_widget_twice_2 	= '';				
			}
		}
		// Load the view's content, with the params passed
		$view_content1 = $this->CI->load->view($widget1, $params, TRUE);
		if($widget2){
			$view_content2 = $this->CI->load->view($widget2, $params, TRUE);
		}else{
			$view_content2 = '';
		}
		// Now load the layout, and pass the view we just rendered
		$this->CI->load->view($layout, array(
			'color_for_row' 		=> $color_for_row,
			'content_for_widget1' 	=> $view_content1,
			'content_for_widget2' 	=> $view_content2,
			'title_for_widget_1' 	=> $title_for_widget_twice_1,
			'title_for_widget_2' 	=> $title_for_widget_twice_2
		));
	}
	
	public function add_include($path, $prepend_base_url = TRUE, $id = FALSE){
		
		if ($prepend_base_url) {
			$this->CI->load->helper('url'); // Load this just to be sure
			$this->file_includes[][] = base_url() . $path;
			$this->file_includes[sizeof($this->file_includes)-1][1] = $id;
		}
		else {
			$this->file_includes[][] = $path;
			$this->file_includes[sizeof($this->file_includes)-1][1] = $id;
		}
		return $this; // This allows chain-methods
	}
	
	public function add_include_footer($path, $prepend_base_url = TRUE, $id = FALSE){

		if ($prepend_base_url) {
			$this->CI->load->helper('url'); // Load this just to be sure
			$this->file_includes_footer[][] = base_url() . $path;
			$this->file_includes_footer[sizeof($this->file_includes_footer)-1][1] = $id;
		}
		else {
			$this->file_includes_footer[][] = $path;
			$this->file_includes_footer[sizeof($this->file_includes_footer)-1][1] = $id;
		}
		return $this; // This allows chain-methods
	}
	
	public function print_includes(){
		
		// Initialize a string that will hold all includes
		$final_includes = '';

		foreach($this->file_includes as $include) {

			// Check if it's a JS or a CSS file
			if (preg_match('/js$/', $include[0]) || preg_match('/plugins$/', $include[0]) || preg_match('/scripts$/', $include[0]) || preg_match('/layouts$/', $include[0])) {
				// It's a JS file
				$final_includes.= '<script type="text/javascript" src="' . $include[0] . '"></script>';
			}
			elseif (preg_match('/css$/', $include[0]) || preg_match('$css$', $include[0])) {

				// It's a CSS file
				if(!$include[1]){
					$final_includes.= '<link href="' . $include[0] . '" rel="stylesheet" type="text/css" />';
				}else{
					$final_includes.= '<link id="' . $include[1] . '" href="' . $include[0] . '" rel="stylesheet" type="text/css" />';
				}
			}
		}
		return $final_includes;
	}
	
	public function print_includes_footer(){
		
		// Initialize a string that will hold all includes
		$final_includes = '';
		foreach($this->file_includes_footer as $include) {
			// Check if it's a JS or a CSS file
			if (preg_match('/js$/', $include[0]) || preg_match('/plugins$/', $include[0]) || preg_match('/scripts$/', $include[0]) || preg_match('/layouts$/', $include[0])) {
				// It's a JS file
				$final_includes.= '<script type="text/javascript" src="' . $include[0] . '"></script>';
			}
			elseif (preg_match('/css$/', $include[0]) || preg_match('$css$', $include[0])) {
				// It's a CSS file
				if(!$include[1]){
					$final_includes.= '<link href="' . $include[0] . '" rel="stylesheet" type="text/css" />';
				}else{
					$final_includes.= '<link id="' . $include[1] . '" href="' . $include[0] . '" rel="stylesheet" type="text/css" />';
				}
			}
		}
		return $final_includes;
	}
	
}