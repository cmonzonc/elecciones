<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

parse_str($_SERVER['QUERY_STRING'], $_GET);

/**
 * Description of users
 * @package home
 * @subpackage controllers
 * @author Monzón Cárdenas, Christian
 * @copyright Monzón Cárdenas, Christian 2016
 */

class Elecciones extends MY_Controller {

    public function __construct() {
	    
        parent::__construct();
		$this->load->library('layouts', 'simple_html_dom');
		$this->load->helper(array('cookie'));
		$this->load->model('elecciones_model');

    }

	public function onpe(){

		$ubigeos = $this->elecciones_model->get_ubigeos();
	
		foreach($ubigeos as $location){
			
			ob_start();		
			
			if((int)$location['ubigeo_dis'] < 100000){
				$t = '0' . $location['ubigeo_dis'];
			}else{
				$t = $location['ubigeo_dis'];
			}
			
			$this->scan_ubigeo($t);
			$this->elecciones_model->update_status($location['ubigeo_dis']);
			echo $location['ubigeo_dis'];
			
			echo ob_get_contents();
			ob_end_flush();
		}
		

	}
	
	public function scan_ubigeo($ubigeo){

		$onpeAddress = "https://resultadoselecciones2016.onpe.gob.pe/PRP2V2016/ajax.php";
		
		$data = array('_accion'=>'displayResultado',
		              '_clase'=>'resultado',
		              'modElec'=>'PR',
		              'pantalla'=>'',
		              'pid'=>'3444817662042101',
		              'subpantalla'=>'Datos',
		              'tipoElec'=>'10',
		              'ubigeo'=>$ubigeo);

 		$query = http_build_query($data);
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $onpeAddress,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_SSL_CIPHER_LIST => 'TLSv1',
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_ENCODING => "",
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $query,
		  CURLOPT_HTTPHEADER => array(
		    "accept: */*",
		    "accept-encoding: gzip, deflate",
		    "accept-language: en-US,en;q=0.8,es;q=0.6",
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		    "cookie: __cfduid=d8b5b2daf2f24fc69717f29f1d2d0fe991460864874; PHPSESSID=hbgknitorne2jgnghguocre2j1; ARRAffinity=401f3c77256e5abf925b4a512b25e03009bae42f63ca0cdde1f9a0bf90236076; __cfduid=d50714a365f95357e8fe1251bb7d4bb471460864954; PHPSESSID=oep6pi7j15043fc3ic7c5415i1; ARRAffinity=d1a7e25c54f9b1286275c6d1633ccf9c2d4ba453ad7e262255842710e9fff6e0",
		    "origin: https//resultadoselecciones2016.onpe.gob.pe",
		    "postman-token: dc4a6bf6-68f0-3e07-2b13-5d73134bbbe4",
		    "referer: https//resultadoselecciones2016.onpe.gob.pe/PRPCP2016/Resultados-Ubigeo-Presidencial.html",
		    "user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36",
		    "x-requested-with: XMLHttpRequest"
		  ),
		));
		
		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);
		
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
// 		  echo $response;
		}
		
		$html = new simple_html_dom();
		$html 	= str_get_html($response);
	

		$tablePoll 	= $html->find('table', 0);
		$tablePoll1 	= $html->find('table', 1);
		$tablePoll2 	= $html->find('table', 2);
		$tablePoll3 	= $html->find('table', 3);
		$tablePoll4 	= $html->find('table', 4);
						
		$resultPoll = $this->_extract_table_information($tablePoll); // Resultado
		$resultPoll1 = $this->_extract_table_information($tablePoll1); // Votos
		$resultPoll2 = $this->_extract_table_information($tablePoll2); // Actas
		$resultPoll3 = $this->_extract_table_information($tablePoll3); // Mesas
		$resultPoll4 = $this->_extract_table_information($tablePoll4);	// Información mesas

		$data = array("ubigeo_dis" => $ubigeo, "ppkVotos" => intval(str_replace(',', '', $resultPoll[1][1])), 
					"ppkVotosValidos" => (float)$resultPoll[1][2], "ppkVotosEmitidos" => (float)$resultPoll[1][3], "fpVotos" => intval(str_replace(',', '', $resultPoll[2][1])), 
					"fpVotosValidos" => (float)$resultPoll[2][2], "fpVotosEmitidos" => (float)$resultPoll[2][3], "blancoVotos" => intval(str_replace(',', '', $resultPoll1[1][1])), 
					"blancoVotosPor" => (float)$resultPoll1[1][3], "nuloVotos" => intval(str_replace(',', '', $resultPoll1[2][1])), "nuloVotosPor" => (float)$resultPoll1[2][3], 
					"actProcesadas" => (int)$resultPoll2[1][1], "actNormales" => (int)$resultPoll2[2][1], "actImpugnados" => (int)$resultPoll2[4][1], 
					"actError" => (int)$resultPoll2[5][1], "actIlegibles"=> (int)$resultPoll2[6][1], "actIncompleto"=> (int)$resultPoll2[7][1], 
					"actSolNulo"=> (int)$resultPoll2[8][1], "actSinData"=> (int)$resultPoll2[9][1], "actExtraviada"=> (int)$resultPoll2[10][1], 
					"actSiniestrada"=> (int)$resultPoll2[11][1], "actSinFirma"=> (int)$resultPoll2[12][1], "actObservada"=> (int)$resultPoll2[13][1], 
					"actAnulada"=> (int)$resultPoll2[15][1], "actNoInstalada" => (int)$resultPoll2[16][1], "actPorProcesar"=> (int)$resultPoll2[17][1]);

		$this->elecciones_model->save_district($data);
	
	}
	
	public function _extract_table_information($table){
		
		// Find all tr tags
		foreach($table->find('tr') as $row) {
		    // initialize array to store the cell data from each row
		    $rowData = array();
		    foreach($row->find('td') as $cell) {
		        $rowData[] = $cell->plaintext;
		    }
		    $theData[] = $rowData;
		}
		
		return $theData;
		
	}

		
}


?>