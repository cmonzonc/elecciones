<?php

/**
 * Load the persistence pages model 
 * @package pages
 * @subpackage models
 * @author Monzón Cárdenas, Christian
 * @copyright Monzón Cárdenas, Christian 2016
 */


class Elecciones_model extends CI_Model {

	var $streaming = 'streaming_invited';

    /**
	* Construct - Instance to parent CIModel
	* @param void
	*/
    public function __construct() {
        parent::__construct();
		$this->db = $this->load->database('default', TRUE);
    }
    
    public function save_district($data){
	    
 		$str 	= $this->db->insert_string("votos", $data);	
 		$str = str_replace('INSERT INTO','INSERT IGNORE INTO',$str);
 		
		$query 	= $this->db->query($str);

    }
    
    public function get_ubigeos(){
	    
	    $str = "SELECT ubigeo_dis FROM distritos WHERE estado = ?";
	    $query = $this->db->query($str, array(0));
	    
	    return $query->result_array();
    }
    
    public function update_status($ubigeo_id){
	    
	    $str = "UPDATE distritos SET estado = ? WHERE ubigeo_dis = ?";
	    $query = $this->db->query($str, array(1, $ubigeo_id));
	    
    }

}

?>
